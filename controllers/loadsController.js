const loadsService = require('../services/loadsService');

module.exports.getAllLoads = async (req, res) => {
  const loads = await loadsService.getAllUsersLoads(
      req.user._id,
      req.user.role,
      req.query.limit,
      req.query.offset);
  res.json({'loads': loads});
};

module.exports.newLoad = async (req, res) => {
  await loadsService.saveNewLoad(req.user._id, req.body);
  res.json({'message': `Success`});
};

module.exports.getLoadById = async (req, res) => {
  const load = await loadsService.getLoadById(req.params.id, req.user._id);
  res.json({'load': load});
};

module.exports.updateLoadById = async (req, res) => {
  await loadsService.updateLoadById(req.params.id, req.user._id, req.body);
  res.json({'message': `Success`});
};

module.exports.changeState = async (req, res) => {
  res.json(await loadsService.changeState(req.user._id));
};

module.exports.deleteLoadById = async (req, res) => {
  await loadsService.deleteLoadById(req.params.id, req.user._id);
  res.json({'message': `Success`});
};

module.exports.postLoadById = async (req, res) => {
  const driverFond = await loadsService.postLoadById(
      req.params.id,
      req.user._id);
  res.json({'message': 'Load posted!', 'driver_fond': driverFond});
};

module.exports.getShippingInfo = async (req, res) => {
  const shippingInfo = await loadsService.getShippingInfo(
      req.params.id,
      req.user._id);
  res.json(shippingInfo);
};

module.exports.getActiveLoads = async (req, res) => {
  res.json({loads: await loadsService.getActiveLoads(req.user._id)});
};
