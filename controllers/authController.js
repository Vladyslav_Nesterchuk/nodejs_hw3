const userService = require('../services/userService');
const authService = require('../services/authService');

module.exports.registration = async (req, res) => {
  await userService.saveNewUser(req.body);
  res.json({message: `User created successfuly!`});
};

module.exports.login = async (req, res) => {
  const token = await authService.login(req.body.email, req.body.password);
  res.json({'message': 'Success', 'jwt_token': token});
};

module.exports.passwordRecowery = async (req, res) => {
  await authService.passwordRecowery(req.body.email);
  res.json({
    'message': `New password sent to your email address`,
  });
};
