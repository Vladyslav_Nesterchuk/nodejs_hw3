const trucksService = require('../services/trucksService');

module.exports.getAllTrucks = async (req, res) => {
  const trucks = await trucksService.getAllUsersTrucks(req.user._id);
  res.json({'trucks': trucks});
};

module.exports.newTruck = async (req, res) => {
  trucksService.saveNewTruck(req.user._id, req.body.type);
  res.json({'message': `Success`});
};

module.exports.getTruckById = async (req, res) => {
  const truck = await trucksService.getTruckById(req.params.id, req.user._id);
  res.json({'truck': truck});
};

module.exports.updateTruckById = async (req, res) => {
  await trucksService.updateTruckById(
      req.params.id,
      req.user._id,
      req.body.type);
  res.json({'message': `Success`});
};

module.exports.assignTruck = async (req, res) => {
  await trucksService.assignTruck(req.params.id, req.user._id);
  res.json({'message': `Success`});
};

module.exports.deleteTruckById = async (req, res) => {
  await trucksService.deleteTruckById(req.params.id, req.user._id);
  res.json({'message': `Success`});
};
