const userServise = require('../services/userService');

module.exports.getProfile = async (req, res) => {
  const user = await userServise.getProfile(req.user._id);
  res.json({
    'user': {
      '_id': user._id,
      'email': user.email,
      'createdDate': user.createdDate,
    },
  });
};

module.exports.changePassword = async (req, res) => {
  await userServise.changePassword(
      req.user._id,
      req.body.oldPassword,
      req.body.newPassword);
  res.json({'message': `Success`});
};

module.exports.deleteProfile = async (req, res) => {
  await userServise.deleteProfile(req.user._id);
  res.json({'message': 'Success'});
};
