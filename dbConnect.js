const mongoose = require('mongoose');
const config = require('./config');

module.exports.mongooseConnect = () => {
  mongoose.connect(config.DB_CONECT, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });
};
