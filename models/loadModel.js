const mongoose = require('mongoose');
const {Schema} = mongoose;
const loadConig = require('../loadConfig');

const loadSchema = new Schema({
  created_by: {
    type: String,
    required: true,
  },
  assignet_to: {
    type: String,
    default: null,
  },
  status: {
    type: String,
    enum: loadConig.statuses,
    default: loadConig.statuses[0],
  },
  state: {
    type: String,
    enum: loadConig.states,
    default: loadConig.states[0],
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    default: 0,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  logs: [
    {message: {
      type: String,
      required: true,
    },
    time: {
      type: Date,
      default: Date.now(),
    },
    },
  ],
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Load = mongoose.model('load', loadSchema);
