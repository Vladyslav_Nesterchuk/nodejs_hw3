const mongoose = require('mongoose');
const {Schema} = mongoose;
const {roles} = require('../userConfig')

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
  role: {
    type: String,
    enum: Object.values(roles),
    required: true,
  },
});

module.exports.User = mongoose.model('User', userSchema);
