const mongoose = require('mongoose');
const {Schema} = mongoose;
const {types, statuses} = require('../truckConfig')

const truckSchema = new Schema({
  created_by: {
    type: String,
    required: true,
  },
  assignet_to: {
    type: String,
    default: null,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
  type: {
    type: String,
    enum: Object.values(types),
    required: true,
  },
  status: {
    type: String,
    enum: Object.values(statuses),
    default: statuses.inService,
  },
});

module.exports.Truck = mongoose.model('Truck', truckSchema);
