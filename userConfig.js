const userRoles = {
  driver: 'DRIVER',
  shipper: 'SHIPPER',
};

module.exports.roles = userRoles;
