const truckStatuses = {
  onLoad: 'OL',
  inService: 'IS'
};

const trucksTypes = {
  sprinter: 'SPRINTER',
  smallStraight:'SMALL STRAIGHT',
  largeStraight:'LARGE STRAIGHT',
};

const trucksSizes = [
  { type: trucksTypes.sprinter,
    width: 300,
    length: 250,
    height: 170,
    payload: 1700,
  },
  { type: trucksTypes.smallStraight,
    width: 500,
    length: 250,
    height: 170,
    payload: 2500,
  },
  { type: trucksTypes.largeStraight,
    width: 700,
    length: 350,
    height: 200,
    payload: 4000,
  }];

module.exports.statuses = truckStatuses;
module.exports.types = trucksTypes;
module.exports.sizes = trucksSizes;
