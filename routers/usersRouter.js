const express = require('express');
const router = new express.Router();
const {validatePasswordChange} = require('./middlewares/validationMiddleware');
const {asyncWrapper} = require('./helpers');
const {
  getProfile,
  changePassword,
  deleteProfile,
} = require('../controllers/usersController');

router.route('/me')
    .get(asyncWrapper(getProfile))
    .patch(
        asyncWrapper(validatePasswordChange),
        asyncWrapper(changePassword))
    .delete(asyncWrapper(deleteProfile));

module.exports = router;
