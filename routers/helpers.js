module.exports.asyncWrapper = function(callback) {
  return (req, res, next) => {
    callback(req, res, next)
        .catch(next);
  };
};

module.exports.errorMapper = function(error) {
  switch (error.name) {
    case 'AuthHeaderError':
    case 'PasswordError':
    case 'TokenError':
    case 'ObjectExistenceError':
    case 'InputValidationError':
    case 'DuplicateError':
    case 'LoadPostEror':
    case 'RolePermissionError':
      return {staus: 400, message: error.message};
    default:
      return {staus: 500, message: 'Internal server error'};
  };
};
