const express = require('express');
const {Router} = express;
const router = new Router();

const {asyncWrapper} = require('./helpers');
const {validateRegistration} = require('./middlewares/validationMiddleware');
const {
  login,
  registration,
  passwordRecowery} = require('../controllers/authController');

router.post('/register',
    asyncWrapper(validateRegistration),
    asyncWrapper(registration));
router.post('/login', asyncWrapper(login));
router.post('/forgot_password', asyncWrapper(passwordRecowery));

module.exports = router;
