const express = require('express');
const router = new express.Router();
const {
  validateTruckData,
  validateId} = require('./middlewares/validationMiddleware');
const {asyncWrapper} = require('./helpers');
const permissionsMiddleware = require('./middlewares/permissionsMiddleware');
const {
  getAllTrucks,
  newTruck,
  getTruckById,
  updateTruckById,
  assignTruck,
  deleteTruckById,
} = require('../controllers/trucksController');
const {roles} = require('../userConfig');

router.use('/', permissionsMiddleware(roles.driver));

router.route('/')
    .get(asyncWrapper(getAllTrucks))
    .post(asyncWrapper(validateTruckData), asyncWrapper(newTruck));

router.route('/:id')
    .all(asyncWrapper(validateId))
    .get(asyncWrapper(getTruckById))
    .delete(asyncWrapper(deleteTruckById))
    .put(asyncWrapper(validateTruckData),
        asyncWrapper(updateTruckById));

router.post('/:id/assign', asyncWrapper(assignTruck));

module.exports = router;
