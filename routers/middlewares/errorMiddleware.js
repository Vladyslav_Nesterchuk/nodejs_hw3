const {errorMapper} = require('../helpers');

module.exports = (err, req, res, next) => {
  console.log(err);
  const errorRes = errorMapper(err);
  res.status(errorRes.staus).json({'message': errorRes.message});
}