const Joi = require('joi');
const passwordRegExp = new RegExp('^[a-zA-Z0-9]{6,30}$');
const objectIDRegExp = new RegExp('^[0-9a-fA-F]{24}$');
const InputValidationError = require('../../errors/InputValidationError');
const registrationSchema = {
  email: Joi.string()
      .email()
      .required()
      .error(new InputValidationError('Email no valid')),
  password: Joi.string()
      .pattern(passwordRegExp)
      .error(new InputValidationError('Password no valid')),
  role: Joi.string()
      .valid('SHIPPER', 'DRIVER')
      .error(new InputValidationError('Role no valid')),
};

const truckDataSchema = {
  type: Joi.string()
      .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
      .required()
      .error(new InputValidationError('Truck type no valid')),
};

const loadDataSchema = {
  name: Joi.string()
      .required()
      .error(new InputValidationError('Load name no valid')),
  payload: Joi.number()
      .required()
      .error(new InputValidationError('Payload no valid')),
  pickup_address: Joi.string()
      .required()
      .error(new InputValidationError('Pickup address no valid')),
  delivery_address: Joi.string()
      .required()
      .error(new InputValidationError('Pickup address no valid')),
  dimensions: Joi.object().keys({
    width: Joi.number()
        .required(),
    length: Joi.number()
        .required(),
    height: Joi.number()
        .required(),
  }).error(new InputValidationError('Dimensions no valid')),
};

const passwordChangeShema = {
  newPassword: Joi.string()
      .required()
      .pattern(passwordRegExp)
      .error(new InputValidationError('New password no valid')),
  oldPassword: Joi.string()
      .required()
      .pattern(passwordRegExp)
      .error(new InputValidationError('Old password no valid')),
};

const idShema = {
  id: Joi.string()
      .pattern(objectIDRegExp)
      .error(new InputValidationError('ID no valid')),
};

/**
 * @param {Object} schemaObj
 * @param {Object} data
 */
async function validate(schemaObj, data) {
  await Joi.object(schemaObj).validateAsync(data);
}

module.exports.validateRegistration = async (req, res, next) => {
  await validate(registrationSchema, req.body);
  next();
};

module.exports.validateTruckData = async (req, res, next) => {
  await validate(truckDataSchema, req.body);
  next();
};

module.exports.validateLoadData = async (req, res, next) => {
  await validate(loadDataSchema, req.body);
  next();
};

module.exports.validatePasswordChange = async (req, res, next) => {
  await validate(passwordChangeShema, req.body);
  next();
};

module.exports.validateId = async (req, res, next) => {
  await validate(idShema, req.params);
  next();
};
