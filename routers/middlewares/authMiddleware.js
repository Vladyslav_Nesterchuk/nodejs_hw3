const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../../config');
const userServise = require('../../services/userService');
const TokenError = require('../../errors/TokenError');
const AuthHeaderError = require('../../errors/AuthHeaderError');

module.exports = async (req, res, next) => {
  const header = req.headers['authorization'];
  if (!header) {
    throw new AuthHeaderError(`No Authorization http header found!`);
  };
  const token = header.split(' ')[1];
  if (!token) {
    throw new AuthHeaderError(`No JWT token found!`);
  };
  try {
    req.user = jwt.verify(token, JWT_SECRET);
    await userServise.getProfile(req.user._id);
  } catch (err) {
    throw new TokenError('No valid token');
  }
  next();
};
