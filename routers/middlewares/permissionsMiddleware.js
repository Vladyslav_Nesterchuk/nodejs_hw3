const RolePermissionError = require('../../errors/RolePermissionError');

module.exports = (...roles) => {
  return (req, res, next) => {
    const {user} = req;
    if (roles.includes(user.role)) {
      next();
    } else {
      throw new RolePermissionError('Access is blocked!');
    };
  };
};
