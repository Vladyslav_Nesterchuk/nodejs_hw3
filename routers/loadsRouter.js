const express = require('express');
const router = new express.Router();
const {
  validateLoadData,
  validateId} = require('./middlewares/validationMiddleware');
const permissionsMiddleware = require('./middlewares/permissionsMiddleware');
const {asyncWrapper} = require('./helpers');
const {
  getAllLoads,
  newLoad,
  getLoadById,
  updateLoadById,
  changeState,
  deleteLoadById,
  postLoadById,
  getActiveLoads,
  getShippingInfo,
} = require('../controllers/loadsController');
const {roles} = require('../userConfig');

router.route('/')
    .get(asyncWrapper(getAllLoads))
    .post(permissionsMiddleware(roles.shipper),
        asyncWrapper(validateLoadData),
        asyncWrapper(newLoad));
router.route('/active')
    .all(permissionsMiddleware(roles.driver))
    .get(asyncWrapper(getActiveLoads));
router.route('/active/state')
    .all(permissionsMiddleware(roles.driver))
    .patch(asyncWrapper(changeState));
router.route('/:id')
    .all(permissionsMiddleware(roles.shipper))
    .all(asyncWrapper(validateId))
    .get(asyncWrapper(getLoadById))
    .delete(asyncWrapper(deleteLoadById))
    .put(asyncWrapper(validateLoadData),
        asyncWrapper(updateLoadById));
router.post('/:id/post',
    permissionsMiddleware(roles.shipper),
    asyncWrapper(postLoadById));
router.get('/:id/shipping_info',
    permissionsMiddleware(roles.shipper),
    asyncWrapper(getShippingInfo));
module.exports = router;
