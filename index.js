const express = require('express');
const morgan = require('morgan');
const config = require('./config');
const dbConnect = require('./dbConnect');
const errorMiddelware = require('./routers/middlewares/errorMiddleware');

const app = express();

const {asyncWrapper} = require('./routers/helpers');
const authMiddleware = require('./routers/middlewares/authMiddleware');

const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const loadsRouter = require('./routers/loadsRouter');
const trucksRouter = require('./routers/trucksRouter');

app.use(express.json());
app.use(express.static('build'));
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', asyncWrapper(authMiddleware), usersRouter);
app.use('/api/trucks', asyncWrapper(authMiddleware), trucksRouter);
app.use('/api/loads', asyncWrapper(authMiddleware), loadsRouter);
app.use('/', (req, res) => {
  res.status(404).json({'message': 'Page not found'});
});

app.use(errorMiddelware);

const start = async () => {
  await dbConnect.mongooseConnect();
  app.listen(config.PORT, () => {
    console.log(`Server works at port ${config.PORT}`);
  });
};

start();
