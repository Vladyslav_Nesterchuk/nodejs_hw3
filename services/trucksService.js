const {Truck} = require('../models/truckModel');
const ObjectExistenceError = require('../errors/ObjectExistenceError');
const truckConfig = require('../truckConfig')
/**
   * @param {Number} width
   * @param {Number} length
   * @param {Number} height
   * @param {Number} payload
   * @return {Array}
   */
function findSuitableTrackType(width, length, height, payload) {
  const typesArr = [];
  truckConfig.sizes.forEach((truck) => {
    if (truck.width >= width &&
      truck.length >= length &&
      truck.height >= height &&
      truck.payload >= payload
    ) {
      typesArr.push(truck.type);
    };
  });
  return typesArr;
}

module.exports.getAllUsersTrucks = async (userId) => {
  return truck = await Truck.find(
      {'created_by': userId},
      {'__v': 0},
  );
};

module.exports.getTruckById = async (truckId, userId) => {
  const truck = await Truck.findOne({
    '_id': truckId,
    'created_by': userId},
  {'__v': 0},
  );
  if (!truck) {
    throw new ObjectExistenceError(`No truck with id: ${truckId}`);
  };
  return truck;
};

module.exports.saveNewTruck = async (userId, truckType) => {
  const truck = new Truck({
    'created_by': userId,
    'type': truckType,
  });
  await truck.save();
};

module.exports.updateTruckById = async (truckId, userId, newType) => {
  const truck = await Truck.findOneAndUpdate({
    '_id': truckId,
    'created_by': userId,
    'assignet_to': null,
  }, {
    'type': newType,
  });
  if (!truck) {
    throw new ObjectExistenceError(`Can't update truck with id: ${truckId}`);
  };
};

module.exports.deleteTruckById = async (truckId, userId) => {
  const truck = await Truck.findOneAndDelete({
    '_id': truckId,
    'created_by': userId,
    'assignet_to': null,
  });
  if (!truck) {
    throw new ObjectExistenceError(`Can't delete truck with id: ${truckId}`);
  };
};

module.exports.getActiveTruck = async (userId) => {
  return await Truck.findOne({'assignet_to': userId}, {'__v': 0});
};

module.exports.assignTruck = async (truckId, userId) => {
  await this.parkCurrentTruck(userId);
  const truck = await Truck.findOneAndUpdate(
      {'created_by': userId, '_id': truckId},
      {'assignet_to': userId});
  if (!truck) {
    throw new ObjectExistenceError(`Can't assign truck with id: ${truckId}`);
  };
};

module.exports.parkCurrentTruck = async (userId) => {
  await Truck.findOneAndUpdate(
      {'assignet_to': userId},
      {'assignet_to': null});
};

module.exports.findSuitableTrack = async (width, length, height, payload) => {
  const types = findSuitableTrackType(width, length, height, payload);
  const truck = await Truck.findOne({
    'status': truckConfig.statuses.inService,
    'assignet_to': {'$ne': null},
    'type': {'$in': types}});
  if (!truck) {
    return null;
  };
  truck.status = truckConfig.statuses.onLoad;
  await truck.save();
  return truck;
};

module.exports.switchTruckStatus = async (userId) => {
  const truck = await this.getActiveTruck(userId);
  truck.status = truckConfig.statuses.inService;
  await truck.save();
};
