const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');
const PasswordErrors = require('../errors/PasswordError');
const ObjectExistenceError = require('../errors/ObjectExistenceError');
const DuplikationError = require('../errors/DuplicateError');

module.exports.saveNewUser = async ({email, password, role}) => {
  const user = new User({
    'email': email,
    'password': await bcrypt.hash(password, 10),
    'role': role,
  });
  await user.save().catch((err) => {
    if (err.code == 11000) {
      throw new DuplikationError('Email already used!');
    };
  });
};

module.exports.getProfile = async (userId, email) => {
  const user = userId ? await User.findById(userId):
                        await User.findOne({'email': email});
  if (!user) {
    throw new ObjectExistenceError(`User not found`);
  };
  return user;
};

module.exports.changePassword = async (userId, oldPassword, newPassword) => {
  const user = await this.getProfile(userId);
  await this.passwordCompare(user._id, oldPassword);
  await User.findOneAndUpdate({
    '_id': userId,
  }, {
    'password': await bcrypt.hash(newPassword, 10),
  });
};

module.exports.deleteProfile = async (userId) => {
  await User.deleteOne({'_id': userId});
};

module.exports.passwordCompare = async (userId, comparPassword) => {
  const user = await this.getProfile(userId);
  if (!await bcrypt.compare(comparPassword, user.password)) {
    throw new PasswordErrors('Wrong password');
  };
};
