const {JWT_SECRET} = require('../config');
const jwt = require('jsonwebtoken');
const userServise = require('./userService');
const ObjectExistenceError = require('../errors/ObjectExistenceError');

module.exports.login = async (email, password) => {
  const user = await userServise.getProfile(null, email);
  await userServise.passwordCompare(user._id, password);
  return jwt.sign({
    'email': user.email,
    '_id': user._id,
    'role': user.role},
  JWT_SECRET);
};

module.exports.passwordRecowery = async (email) => {
  try {
    await userServise.getProfile(null, email);
  } catch (err) {
    throw new ObjectExistenceError('Email not registered!');
  };
};
