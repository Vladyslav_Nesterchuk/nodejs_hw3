const {Load} = require('../models/loadModel');
const ObjectExistenceError = require('../errors/ObjectExistenceError');
const LoadPostError = require('../errors/LoadPostEror');
const trucksService = require('../services/trucksService');
const loadConfig = require('../loadConfig');
const userConfig = require('../userConfig');
const paginationSettings = require('./paginationSettings')

/**
 * @param {*} load
 * @param {*} ifBackToNew
 */
function switchStatus(load, ifBackToNew) {
  const previouStatusIndex = loadConfig.statuses.indexOf(load.status);
  const nextStatusIndex = ifBackToNew ? previouStatusIndex - 1 : previouStatusIndex + 1;
  load.status = loadConfig.statuses[nextStatusIndex];
};

/**
 * @param {*} load
 */
function switchState(load) {
  const previouStateIndex = loadConfig.states.indexOf(load.state);
  load.state = loadConfig.states[previouStateIndex + 1];
};

module.exports.getAllUsersLoads = async (userId, userRole, limit, skip) => {
  findParam = userRole == userConfig.roles.shipper ? 
      {'created_by': userId} :
      {'assignet_to': userId};
  return await Load.find(
      findParam,
      {'__v': 0},
      paginationSettings.formatParameters(limit, skip),
  );
};

module.exports.getLoadById = async (loadId, userId) => {
  const load = await Load.findOne({
    '_id': loadId,
    'userId': userId},
  {'__v': 0},
  );
  if (!load) {
    throw new ObjectExistenceError(`No Load with id: ${loadId}`);
  };
  return load;
};

module.exports.saveNewLoad = async (userId, loadData) => {
  loadData.created_by = userId;
  const load = new Load(loadData);
  await load.save();
};

module.exports.updateLoadById = async (loadId, userId, loadData) => {
  console.log(loadData);
  const load = await Load.findOneAndUpdate({
    '_id': loadId,
    'created_by': userId,
    'status': loadConfig.statuses[0],
  }, loadData);
  if (!load) {
    throw new ObjectExistenceError(`No NEW load with id: ${loadId}`);
  };
};

module.exports.deleteLoadById = async (loadId, userId) => {
  const load = await Load.findOneAndDelete({
    '_id': loadId,
    'created_by': userId,
    'status': loadConfig.statuses[0],
  });
  if (!load) {
    throw new ObjectExistenceError(`No available load with id: ${userId}`);
  };
};

module.exports.changeState = async (userId) => {
  const load = await this.getActiveLoads(userId);
  if (!load) {
    throw new ObjectExistenceError('No active loads');
  };
  switchState(load);
  load.logs.push({'message': `load state changed to ${load.state}`});
  if (load.state == loadConfig.statuses[4]) {
    switchStatus(load);
    load.assignet_to = null;
    await trucksService.switchTruckStatus(userId);
  };
  await load.save();
  return {'message': `Load state changed to \'${load.state}\'`};
};

module.exports.postLoadById = async (loadId, userId) => {
  const load = await Load.findOne({
    '_id': loadId,
    'created_by': userId},
  {'__v': 0},
  );
  if (!load || load.status !== loadConfig.statuses[0]) {
    throw new LoadPostError('No load exist or load already posted');
  };
  switchStatus(load);
  await load.save();
  const selectedTruck = await trucksService.findSuitableTrack(
      ...Object.values( load.dimensions), load.payload);
  if (!selectedTruck) {
    switchStatus(load, true);
    await load.save();
    return false;
  };
  load.assignet_to = selectedTruck.assignet_to;
  load.logs.push({
    'message': `load assignet to driver with id ${load.assignet_to}`,
  });
  switchStatus(load);
  switchState(load);
  await load.save();
  return true;
};

module.exports.getShippingInfo = async (loadId, userId) => {
  const load = await Load.findOne({
    '_id': loadId,
    'created_by': userId,
    'status': loadConfig.statuses[2]},
  {'__v': 0});
  if (!load) {
    throw new ObjectExistenceError(`No acive load with id ${loadId}`);
  };
  const truck = await trucksService.getActiveTruck(load.assignet_to);
  return {load, truck};
};

module.exports.getActiveLoads = async (userId) => {
  return await Load.findOne({'assignet_to': userId}, {'__v': 0});
};
