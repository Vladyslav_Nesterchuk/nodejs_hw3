module.exports.formatParameters = (limit, skip) => {
  limit = +limit || 10;
  skip = +skip || 0;
  if (limit > 50) {
    limit = 50;
  };
  return {limit: limit, skip: skip}
}
