const loadStates = [
  'Awaiting publication',
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery'
];

const loadStatuses = [
  'NEW',
  'POSTED', 
  'ASSIGNED', 
  'SHIPPED'
];

module.exports.states = loadStates;
module.exports.statuses = loadStatuses;
